﻿namespace CroissantWABackEnd.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetPublishDateAsNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EntryModels", "publishDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EntryModels", "publishDate", c => c.DateTime(nullable: false));
        }
    }
}
