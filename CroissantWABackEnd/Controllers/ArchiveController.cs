﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CroissantWABackEnd.Models;

namespace CroissantWABackEnd.Controllers
{
    public class ArchiveController : Controller
    {
        ApplicationDbContext dbContext;

        public ArchiveController()
        {
            dbContext = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            dbContext.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            //TODO 
            return View();
        }
    }
}