﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CroissantWABackEnd.Models
{
    public class EntryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public DateTime createDate { get; set; }
        public DateTime? publishDate { get; set; }
        public string Content { get; set; }
        public List<string> tag { get; set; }
        public bool IsPublished { get; set; }
        
        public EntryModel()
        {
            IsPublished = false;
        }
    }
}